package servicio;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import acceso.Cliente;
import acceso.GestorClientes;

import com.google.gson.JsonArray;

import java.util.ArrayList;

public class AgenciaServer implements Runnable {

    private final GestorClientes gestor;

    private static ServerSocket ssock;
    private static Socket socket;

    private Scanner entradaDecorada;
    private PrintStream salidaDecorada;
    private static final int PUERTO = 5000;

    /**
     * Constructor
     */
    public AgenciaServer() {
        gestor = new GestorClientes();
    }

    /**
     * Logica completa del servidor
     */
    public void iniciar() {
        abrirPuerto();

        while (true) {
            esperarAlCliente();
            lanzarHilo();
        }
    }

    /**
     * Lanza el hilo
     */
    private static void lanzarHilo() {
        new Thread(new AgenciaServer()).start();
    }

    private static void abrirPuerto() {
        try {
            ssock = new ServerSocket(PUERTO);
            System.out.println("Escuchando por el puerto " + PUERTO);
        } catch (IOException ex) {
            Logger.getLogger(AgenciaServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Espera que el cliente se conecta y le devuelve un socket
     */
    private static void esperarAlCliente() {
        try {
            socket = ssock.accept();
            System.out.println("Cliente conectado");
        } catch (IOException ex) {
            Logger.getLogger(AgenciaServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Cuerpo del hilo
     */
    @Override
    public void run() {
        try {
            crearFlujos();
            leerFlujos();
            cerrarFlujos();

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Crea los flujos con el socket
     *
     * @throws IOException
     */
    private void crearFlujos() throws IOException {
        salidaDecorada = new PrintStream(socket.getOutputStream());
        entradaDecorada = new Scanner(socket.getInputStream());
    }

    /**
     * Lee los flujos del socket
     */
    private void leerFlujos() {
        if (entradaDecorada.hasNextLine()) {
            // Extrae el flujo que envía el cliente
            String peticion = entradaDecorada.nextLine();
            decodificarPeticion(peticion);

        } else {
            salidaDecorada.flush();
            salidaDecorada.println("NO_ENCONTRADO");
        }
    }

    /**
     * Decodifica la petición, extrayeno la acción y los parámetros
     *
     * @param peticion petición completa al estilo
     * "consultarCiudadano,983932814"
     */
    private void decodificarPeticion(String peticion) {
        StringTokenizer tokens = new StringTokenizer(peticion, ",");
        String parametros[] = new String[10];

        int i = 0;
        while (tokens.hasMoreTokens()) {
            parametros[i++] = tokens.nextToken();
        }
        String accion = parametros[0];
        procesarAccion(accion, parametros);

    }

    /**
     * Segun el protocolo decide qué accion invocar
     *
     * @param accion acción a procesar
     * @param parametros parámetros de la acción
     */
    private void procesarAccion(String accion, String parametros[]) {
        switch (accion) {
            case "consultarClientesPotenciales":
                String id = parametros[1];
                ArrayList<Cliente> lista = gestor.buscarCliente(id);

                if (lista.size() == 0) {
                    salidaDecorada.println("NO_ENCONTRADO");
                } else {
                    salidaDecorada.println(parseToJSONCliente(lista));
                }
                break;
            case "consultarPaquetesActuales":
                ArrayList<InfoPaquete> paquetes = gestor.buscarPaquetes();
                if (paquetes.size() == 0) {
                    salidaDecorada.println("NO_ENCONTRADO");
                } else {
                    salidaDecorada.println(parseToJSONPaquetes(paquetes));
                }
        }
    }

    /**
     * Cierra los flujos de entrada y salida
     *
     * @throws IOException
     */
    private void cerrarFlujos() throws IOException {
        salidaDecorada.close();
        entradaDecorada.close();
        socket.close();
    }

    /**
     * Convierte el objeto Ciudadano a json
     *
     * @param cliente Objeto ciudadano
     * @return cadena json
     */
    private String parseToJSONCliente(ArrayList<Cliente> lista) {
        JsonObject jsonobj;
        JsonArray jsonLista = new JsonArray();
        for (Cliente tem : lista) {
            jsonobj = new JsonObject();
            jsonobj.addProperty("cedula", tem.getCedula());
            jsonobj.addProperty("nombres", tem.getNombres());
            jsonobj.addProperty("apellidos", tem.getApellidos());
            jsonobj.addProperty("email", tem.getEmail());
            jsonobj.addProperty("sexo", tem.getSexo());
            jsonobj.addProperty("edad", tem.getEdad());
            jsonLista.add(jsonobj);

        }

        return jsonLista.toString();
    }

    private String parseToJSONPaquetes(ArrayList<InfoPaquete> paquetes) {

        JsonArray array = new JsonArray();
        JsonObject jsonobj;
        for (InfoPaquete paq : paquetes) {
            jsonobj = new JsonObject();
            jsonobj.addProperty("cod", paq.getCod());
            jsonobj.addProperty("nombre", paq.getNombre());
            jsonobj.addProperty("descripcion", paq.getDescripcion());
            jsonobj.addProperty("edad_base", paq.getEdad_base());
            jsonobj.addProperty("edad_tope", paq.getEdad_tope());
            jsonobj.addProperty("genero", paq.getGenero());
            
            
            array.add(jsonobj);
        }

        return array.toString();
    }

}
