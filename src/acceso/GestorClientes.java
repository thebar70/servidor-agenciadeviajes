/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

import java.util.ArrayList;
import servicio.InfoPaquete;

/**
 *
 * @author thebar70
 */
public class GestorClientes {

    private ConectorJdbc conector;

    public GestorClientes() {
        conector = new ConectorJdbc();
    }

    public ArrayList<Cliente> buscarCliente(String id_paquete) {
        try {
            conector.conectarse();
            InfoPaquete plan = consultarPaquete(id_paquete);
            String sql = crarConsultaClientes1(plan);
            conector.crearConsulta(sql);
            Cliente cliente = null;
            ArrayList<Cliente> lista = new ArrayList<>();
            while (conector.getResultado().next()) {
                cliente = new Cliente(conector.getResultado().getString("cedula"),
                        conector.getResultado().getString("nombres"),
                        conector.getResultado().getString("apellidos"),
                        conector.getResultado().getString("email"),
                        conector.getResultado().getString("sexo"),
                        conector.getResultado().getString("edad"));
                lista.add(cliente);
            }
            conector.desconectarse();
            return lista;
        } catch (Exception e) {
            System.out.println("error: " + e);
            return null;
        }

    }

    private InfoPaquete consultarPaquete(String info_paquete) {
        InfoPaquete paq = new InfoPaquete();
        try {
            conector.conectarse();
            conector.crearConsulta("SELECT * FROM PAQUETES pq where pq.id="+info_paquete);
       
            while (conector.getResultado().next()) {
                paq.setCod(Integer.parseInt(conector.getResultado().getString("id")));
                paq.setNombre(conector.getResultado().getString("nombre"));
                paq.setDescripcion(conector.getResultado().getString("descripcion"));
                paq.setEdad_base(conector.getResultado().getString("edadminimaaplicable"));
                paq.setEdad_tope(conector.getResultado().getString("edadmaximaaplicable"));
                paq.setGenero(conector.getResultado().getString("genero"));
            }

        } catch (Exception e) {
            System.out.println("Error al creoar el paquete"+e);
        }
        return paq;
    }

    public ArrayList<InfoPaquete> buscarPaquetes() {
        ArrayList<InfoPaquete> paquetes = new ArrayList<>();
        try {

            conector.conectarse();
            conector.crearConsulta("SELECT * FROM PAQUETES");
            InfoPaquete paq = new InfoPaquete();
            while (conector.getResultado().next()) {
                paq.setCod(Integer.parseInt(conector.getResultado().getString("id")));
                paq.setNombre(conector.getResultado().getString("nombre"));
                paq.setDescripcion(conector.getResultado().getString("descripcion"));
                paq.setEdad_base(conector.getResultado().getString("edadminimaaplicable"));
                paq.setEdad_tope(conector.getResultado().getString("edadmaximaaplicable"));
                paq.setGenero(conector.getResultado().getString("genero"));

                paquetes.add(paq);
                paq = new InfoPaquete();
            }
        } catch (Exception e) {
            System.out.println("Errro al traer los paquetes");
        }
        return paquetes;
    }

    private String crarConsultaClientes(String id_paquete) {
        String sql = "SELECT cli.cedula, cli.nombres, cli.apellidos,cli.email,cli.sexo,"
                + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE()) as edad"
                + " FROM clientes cli where "
                + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())>="
                + "(select pq.edadminimaaplicable from paquetes pq where pq.ID=" + id_paquete + ") and "
                + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())<="
                + "(select pq.edadmaximaaplicable from paquetes pq where pq.ID=" + id_paquete + ") and "
                + "(select pq.GENERO FROM paquetes pq where pq.ID=" + id_paquete + ")=cli.SEXO or";
        return sql;
    }

    private String crarConsultaClientes1(InfoPaquete plan) {
        String sql;
        if (plan.getGenero().equals("T")) {
            sql = "SELECT cli.cedula, cli.nombres, cli.apellidos,cli.email,cli.sexo,"
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE()) as edad"
                    + " FROM clientes cli where "
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())>="
                    + "(select pq.edadminimaaplicable from paquetes pq where pq.ID=" + plan.getCod() + ") and "
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())<="
                    + "(select pq.edadmaximaaplicable from paquetes pq where pq.ID=" + plan.getCod() + ")";
        } else {
            sql = "SELECT cli.cedula, cli.nombres, cli.apellidos,cli.email,cli.sexo,"
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE()) as edad"
                    + " FROM clientes cli where "
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())>="
                    + "(select pq.edadminimaaplicable from paquetes pq where pq.ID=" + plan.getCod() + ") and "
                    + "TIMESTAMPDIFF(YEAR,CAST(cli.FECHANACIMIENTO as TIMESTAMP),CURDATE())<="
                    + "(select pq.edadmaximaaplicable from paquetes pq where pq.ID=" + plan.getCod() + ") and "
                    + "(select pq.GENERO FROM paquetes pq where pq.ID=" + plan.getCod() + ")=cli.SEXO";
        }

        return sql;
    }
}
